const { BN, constants, expectEvent, expectRevert } = require('openzeppelin-test-helpers');
const { expect } = require('chai');
const SGXProgramStore = artifacts.require("SGXProgramStore");
const USDT = artifacts.require("USDT");
const {StepRecorder} = require("../util.js");
const SGXClassifierStore = artifacts.require("SGXClassifierStore")
const SGXClassifierMarket = artifacts.require("SGXClassifierMarket")

contract('TESTSGXClassifyRequest', (accounts) => {

  let program_store = {}
  let market = {}
  let token = {}
  let classifier_store = {}


  let pkey = {};
  let program_hash = {}
  let data_hash = {}
  let data_vhash = {}
  let request_hash = {}
  let pstore = {}

	context('init', async () => {
    it('init', async()=>{
      //assert.ok(token);
      sr = StepRecorder('ganache', 'test')
      token = await USDT.at(sr.value('token'));
      //await token.generateTokens(accounts[0], 1000000000);
      classifier_store = await SGXClassifierStore.at(sr.value("classifier-store"))
      market = await SGXClassifierMarket.at(sr.value("market"))
      pstore = await classifier_store.program_store()
      pstore = await SGXProgramStore.at(pstore)
    })

    it('upload program', async() =>{
      tx = await classifier_store.upload_model("test_url", 0,
        "0xc3bdf4e01586f133bb07a8755f841b275c27697e148495c3edb716aa53de9d33",
        "0x72dcbda811124610b130238cb9281d3ca4e9c3eed268d89e48f361e668d62c09",
        "0xbb51aa890e5eaf4d7a88200136adef44d00f03a8d55c532382439f1363a4af7d4e4b22adbf122ce2e4af326d0b25649b793705ed9ac823172c47fc1381aa1738")
      console.log("tx logs: ", tx.logs)
      program_hash = tx.logs[0].args.hash
      b = await pstore.is_program_hash_available(program_hash)
    })

    it('submit request', async() =>{

      param_hash = "0x2c36761fac78ea78e74bfb880c23210b20bda2b003d5987c4ba5e94917db66d5"
      dian_pkey = "0xb72b734f80335d96da5822f912badc628a1e31916bb60d79072aeabb998f9a4812c87cb22b87a8b970a640bae6e0b5bfe973f7eace09747948748c130d09c724"
      amount = 0
      tx = await market.requestModel(program_hash, param_hash, dian_pkey, amount)
      request_hash = tx.logs[0].args.request_hash
    })

    it('submit allowance', async() =>{
      let result = "0x7cb9170af2e87c9f8a8f440dc37cea1c40b83bd215af5314b5a7a684d7679ad245bf92988e8c418ef8410dc1a99583237678ebaba3e2435a3ab9c9c3cd3380131b"
      tx = await market.grantModel(request_hash, result, "hehe, no use")
    })
	})
});
