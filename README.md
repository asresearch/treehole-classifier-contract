# treehole-classifier-contract
This is for classification or predication with Fidelius.
The scenario is that the model needs to be protected.
Say a user downloaded the model and the program, the model author may
want an ensurance that the model can only be used with permission, i.e.,
for specific input.


## Getting started

This project is not designed to be deployed standalone.
The `SGXClassifierStore` contract is on top of `SGXProgramStore`,
which should be deployed in the other project.

Notice that the other project is with solidity 0.5.10, yet this project is
with solidity 0.8.10. That's why we couldn't put them into one project.


However, you may test and run it standalone, since we have put the
dependencies in the `contracts/test/` directory.

```
npm install
truffle compile --all
truffle migrate --network ganache
truffle test --network ganache
```

## TODO
There are still some future works, including but not limited to

  1. Testing cases for token payment, cancel request, transfer
     request ownership .
  2. Testing cases for setting up parameters, like change fee.

## Contributing
<!--State if you are open to contributions and what your requirements are for accepting them.-->

<!--For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.-->

<!--You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.-->

## Authors and acknowledgment
<!--Show your appreciation to those who have contributed to the project.-->

## License
<!--For open source projects, say how it is licensed.-->

## Project status
<!--If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.-->
