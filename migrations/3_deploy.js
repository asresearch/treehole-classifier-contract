const SGXClassifierStore = artifacts.require("SGXClassifierStore")
const SGXClassifierMarket = artifacts.require("SGXClassifierMarket")
const {DHelper, StepRecorder} = require("../util.js");
const { deployProxy  } = require('@openzeppelin/truffle-upgrades');

async function performMigration(deployer, network, accounts, dhelper) {
  test = StepRecorder(network, "test")
  oproxy = test.read('owner')
  store = test.read('program-store')
  usdt = test.read('token')
  cstore = await deployProxy(SGXClassifierStore, [store, oproxy], {deployer})
  market = await deployProxy(SGXClassifierMarket, [usdt, accounts[0],oproxy], {deployer})
  await market.grantRole(await market.OWNER_ROLE(), accounts[0])
  await market.changeClassifierStore(cstore.address)
  test.write('classifier-store', cstore.address)
  test.write('market', market.address)
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
