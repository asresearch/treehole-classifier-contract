const {DHelper, StepRecorder} = require("../util.js");
const { deployProxy  } = require('@openzeppelin/truffle-upgrades');
const NaiveOwner = artifacts.require("NaiveOwner")
const SGXProgramStore = artifacts.require("SGXProgramStore")
const USDT = artifacts.require("USDT")

async function performMigration(deployer, network, accounts, dhelper) {
  owner_proxy = await deployProxy(NaiveOwner, [], {deployer});
  store = await dhelper.readOrCreateContract(SGXProgramStore, [], owner_proxy.address)
  usdt = await dhelper.readOrCreateContract(USDT, [])
  sr = StepRecorder(network, "test");
  sr.write("owner", owner_proxy.address)
  sr.write("program-store", store.address)
  sr.write('token', usdt.address)
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
