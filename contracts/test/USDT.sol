// SPDX-License-Identifier: MIT
pragma solidity >=0.8.10;
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract USDT is ERC20{
  constructor() ERC20("USD Tether", "USDT"){
  }

  function decimals() public view virtual override returns(uint8){
    return 8;
  }

  function issue(address account, uint256 num) public{
    _mint(account, num);
  }
}
