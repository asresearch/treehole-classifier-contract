// SPDX-License-Identifier: MIT
pragma solidity >=0.8.10;
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "./interface/OwnerProxyInterface.sol";
import "./interface/SGXProgramStoreInterface.sol";

contract SGXClassifierStore is Initializable{
  struct model_info{
    bytes32 model_hash;
    bytes model_pkey;
  }
  mapping(bytes32=>model_info) public all_model_info;
  SGXProgramStoreInterface public program_store;
  OwnerProxyInterface public owner_proxy;

  function initialize(address _program_store, address _owner_proxy) public initializer{
    program_store = SGXProgramStoreInterface(_program_store);
    owner_proxy = OwnerProxyInterface(_owner_proxy);
  }

  event UploadModel(bytes32 hash, bytes32 model_hash, bytes pkey);
  //event from internal
  event UploadProgram(bytes32 hash, address author);
  function upload_model(string memory _url, uint256 _price,
                          bytes32 _enclave_hash, bytes32 _model_hash,
                         bytes memory _pkey) public returns(bytes32){
    bytes32 h = program_store.upload_program(_url, _price, _enclave_hash);
    all_model_info[h].model_hash = _model_hash;
    all_model_info[h].model_pkey= _pkey; owner_proxy.transferOwnership(h, msg.sender);
    emit UploadModel(h, _model_hash, _pkey);
    return h;
  }

  function modelOfProgram(bytes32 h) public view returns(bytes32, bytes memory) {
    return (all_model_info[h].model_hash, all_model_info[h].model_pkey);
  }

  function is_program_hash_available(bytes32 hash) external view returns(bool){
    return program_store.is_program_hash_available(hash);
  }
  function program_price(bytes32 hash) external view returns(uint256){
    return program_store.program_price(hash);
  }
  function program_owner(bytes32 hash) external view returns(address){
    return program_store.program_owner(hash);
  }
  function enclave_hash(bytes32 hash) external view returns(bytes32){
    return program_store.enclave_hash(hash);
  }
}
