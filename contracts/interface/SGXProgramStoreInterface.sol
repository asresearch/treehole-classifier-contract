// SPDX-License-Identifier: MIT
pragma solidity >=0.8.10;

interface SGXProgramStoreInterface{

  function upload_program(string memory _url, uint256 _price, bytes32 _enclave_hash) external returns(bytes32);
  function is_program_hash_available(bytes32 hash) external view returns(bool);
  function program_price(bytes32 hash) external view returns(uint256);
  function program_owner(bytes32 hash) external view returns(address);
  function enclave_hash(bytes32 hash) external view returns(bytes32);
}
