// SPDX-License-Identifier: MIT
pragma solidity >=0.8.10;
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/access/AccessControlUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/cryptography/ECDSAUpgradeable.sol";
import "./utils/SignatureVerifierUpgradeable.sol";
import "./interface/SGXClassifierStoreInterface.sol";
import "./interface/OwnerProxyInterface.sol";

contract SGXClassifierMarket is Initializable, AccessControlUpgradeable{
  using SafeERC20Upgradeable for IERC20Upgradeable;
  using SignatureVerifierUpgradeable for bytes32;
  using ECDSAUpgradeable for bytes32;

  SGXClassifierStoreInterface public classifier_store;
  bytes32 public constant OWNER_ROLE = keccak256("OWNER_ROLE");
  uint256 public constant ratio_base = 10000;
  uint256 public fee_ratio;
  address public fee_pool;
  uint256 public interval_time;
  OwnerProxyInterface public owner_proxy;

  IERC20Upgradeable public payment_token;

  enum request_status{
    init,
    accepted,
    canceled
  }

  struct request_info{
    bytes32 program_hash;
    bytes32 param_hash;
    bytes dian_pkey;
    uint256 price;
    uint256 fee;
    address owner;
    uint256 start_time;
    uint256 end_time;
    request_status status;
    bool exist;
  }

  mapping(bytes32 => request_info) public all_requests;


  function initialize(address _token, address _fee_pool, address _owner_proxy) public initializer{
    __AccessControl_init();
    _setupRole(DEFAULT_ADMIN_ROLE, msg.sender);
    payment_token = IERC20Upgradeable(_token);
    fee_pool = _fee_pool;
    fee_ratio = 1000; //default is 10%
    interval_time = 7 days; //default
    owner_proxy = OwnerProxyInterface(_owner_proxy);
  }

  event ChangeClassifierStore(address old_store, address new_store);
  function changeClassifierStore(address new_store) public onlyRole(OWNER_ROLE){
    address old = address(classifier_store);
    classifier_store = SGXClassifierStoreInterface(new_store);
    emit ChangeClassifierStore(old, new_store);
  }
  event ChangeIntervalTime(uint256 old_interval_time, uint256 new_interval_time);
  function changeIntervalTime(uint256 _interval_time) public onlyRole(OWNER_ROLE){
    uint256 old = interval_time;
    interval_time = _interval_time;
    emit ChangeIntervalTime(old, interval_time);
  }

  event ChangeFee(uint256 fee_ratio, address fee_pool);
  function changeFee(uint256 _fee, address _fee_pool) public onlyRole(OWNER_ROLE){
    fee_ratio = _fee;
    require(fee_ratio < ratio_base, "invalid fee ratio");
    fee_pool = _fee_pool;
    emit ChangeFee(_fee, _fee_pool);
  }

  event ChangePaymentToken(address payment_token);
  function changePaymentToken(address _payment_token) public onlyRole(OWNER_ROLE){
    payment_token = IERC20Upgradeable(_payment_token);
    emit ChangePaymentToken(_payment_token);
  }

  event RequestModel(bytes32 request_hash, bytes32 mode_hash, bytes32 param_hash, bytes dian_pkey, uint256 amount);
  function requestModel(bytes32 vhash, bytes32 param_hash,
                        bytes memory dian_pkey, uint256 amount) public returns(bytes32){
    bytes32 h = keccak256(abi.encodePacked(vhash, param_hash, dian_pkey, amount, msg.sender, block.number));
    request_info storage ri = all_requests[h];

    require(!ri.exist, "request already exist");
    require(classifier_store.is_program_hash_available(vhash), "program hash not found");

    ri.exist = true;
    ri.program_hash = vhash;
    ri.param_hash = param_hash;
    ri.dian_pkey = dian_pkey;
    ri.owner = msg.sender;
    ri.price = classifier_store.program_price(vhash);
    ri.start_time = block.timestamp;
    ri.end_time = block.timestamp + interval_time;
    ri.status = request_status.init;

    if(payment_token != IERC20Upgradeable(address(0x0)) && amount != 0){
      payment_token.safeTransferFrom(msg.sender, address(this), amount);
      uint256 expect = amount*(ratio_base + fee_ratio)/ratio_base;
      ri.fee = expect - amount;

      if(amount > expect){
        uint256 exchange = amount - expect;
        payment_token.safeTransfer(msg.sender, exchange);
      }
    }

    emit RequestModel(h, vhash, param_hash, dian_pkey, amount);
    return h;
  }

  function getRequestInfo(bytes32 request_hash) public view returns(request_info memory){
    return all_requests[request_hash];
  }

  event TransferRequestOwner(bytes32 request_hash, address new_owner);
  function transferRequestOwner(bytes32 request_hash, address _new_owner) public{
    request_info storage ri = all_requests[request_hash];
    require(ri.owner == msg.sender, "only for request owner");
    ri.owner = _new_owner;
    emit TransferRequestOwner(request_hash, _new_owner);
  }

  event GrantModel(bytes32 request_hash, bytes allowance, string extra_uri);
  function grantModel(bytes32 request_hash, bytes memory allowance, string memory extra_uri) public{
    request_info storage ri = all_requests[request_hash];
    require(ri.exist, "only for existed request");
    require(ri.status == request_status.init, "only for init status request");
    ri.status = request_status.accepted;

    if(payment_token != IERC20Upgradeable(address(0x0))){
      address model_owner = owner_proxy.ownerOf(ri.program_hash);
      payment_token.safeTransfer(model_owner, ri.price);
      payment_token.safeTransfer(fee_pool, ri.fee);
    }

    bytes32 hash;
    bytes32 model_hash;
    bytes memory pkey;
    {
      bytes32 enclave_hash = classifier_store.enclave_hash(ri.program_hash);
      (model_hash, pkey) = classifier_store.modelOfProgram(ri.program_hash);
      hash = keccak256(abi.encodePacked(ri.param_hash, enclave_hash, ri.dian_pkey, model_hash));
    }
    bool verified = hash.toEthSignedMessageHash().verify_signature(allowance, pkey);
    require(verified, "invalid allowance");
    emit GrantModel(request_hash, allowance, extra_uri);
  }

  event CancelRequest(bytes32 request_hash);
  function cancelRequest(bytes32 request_hash) public{
    request_info storage ri = all_requests[request_hash];
    require(ri.exist, "only for existed request");
    require(block.timestamp > ri.end_time, "not ready to cancel");
    require(ri.owner == msg.sender, "only for request owner");
    require(ri.status == request_status.init, "only for init status request");
    ri.status = request_status.canceled;

    if(payment_token != IERC20Upgradeable(address(0x0))){
      payment_token.safeTransfer(msg.sender, ri.price + ri.fee);
    }
    emit CancelRequest(request_hash);
  }

  event TransferAdministrator(address new_owner);
  function transferAdministrator(address new_owner) public onlyRole(DEFAULT_ADMIN_ROLE){
    _grantRole(DEFAULT_ADMIN_ROLE, new_owner);
    _revokeRole(DEFAULT_ADMIN_ROLE, msg.sender);
    emit TransferAdministrator(new_owner);
  }

}
